# Node templates catalog

## Setup
Add the directory to your PATH

## Usage

### Get the list of templates:
```bash
node-list
```

### Create a new application
```bash
node-new <template name>
```
> This will create a new git repository based on the chosen template.
